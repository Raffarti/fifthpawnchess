# Intended for gitlab CI

SHELL := /bin/bash
QT5_DIR ?= /tmp/qt5
QT5_BUILD_DIR ?= /tmp/qt5build
NDK ?= $(PWD)/android-ndk-r20
SDK ?= $(PWD)/android-sdk

.PHONY: pages

.ONESHELL:
$(QT5_DIR)/5.15.2/android/mkspecs/modules/qt_lib_networkauth.pri: | $(QT5_DIR)/5.15.2/android/bin/qmake $(NDK) $(SDK)/platforms/android-26
	export ANDROID_SDK_ROOT=$(SDK) ANDROID_NDK_ROOT="$(NDK)"
	cd $(QT5_DIR)
	git clone git://code.qt.io/qt/qtnetworkauth.git
	cd qtnetworkauth
	$(QT5_DIR)/5.15.2/android/bin/qmake
	make
	make install

$(QT5_DIR):
	mkdir -p $(QT5_DIR)

.SECONDARY:
$(QT5_BUILD_DIR):
	mkdir -p $(QT5_BUILD_DIR)

install-qt.sh:
	curl -o install-qt.sh https://code.qt.io/cgit/qbs/qbs.git/plain/scripts/install-qt.sh
	chmod +x install-qt.sh
	sed -i.bak -e s/QT_VERSION/VERSION/ install-qt.sh

.ONESHELL:
$(QT5_DIR)/5.15.2/wasm_32/bin/qmake: | $(QT5_BUILD_DIR) ~/.emscripten
	source emsdk/emsdk_env.sh
	mkdir -p $(QT5_BUILD_DIR)
	cd $(QT5_BUILD_DIR)
	git clone git://code.qt.io/qt/qt5.git
	cd qt5
	git checkout 5.15.2
	perl init-repository --module-subset=qtbase,qtdeclarative,qtgraphicaleffects,qtimageformats,qtnetworkauth,qtquickcontrols2,qtscxml,qtsvg,qtxmlpatterns
	mkdir -p build_wasm && cd build_wasm
	../configure -xplatform wasm-emscripten -nomake examples -nomake tests -opensource -prefix $(QT5_DIR)/5.15.2/wasm_32 -confirm-license -feature-http -feature-thread
	make -j $(shell nproc)
	make install

emsdk:
	git clone https://github.com/emscripten-core/emsdk.git

emsdk/upstream: | emsdk
	emsdk/emsdk install 1.39.8
	emsdk/emsdk install fastcomp-clang-e1.38.30-64bit

~/.emscripten: | emsdk/upstream
	emsdk/emsdk activate 1.39.8

.ONESHELL:
pages: | ~/.emscripten $(QT5_DIR)/5.15.2/wasm_32/bin/qmake
	source emsdk/emsdk_env.sh
	lrelease-qt5 FifthPawnChess.pro
	$(QT5_DIR)/5.15.2/wasm_32/bin/qmake
	make
	patch < icon.patch
	mv qtlogo.svg qtloader.js FifthPawnChess.js FifthPawnChess.worker.js FifthPawnChess.html FifthPawnChess.wasm FifthPawnChess.wasm.map Icon.svg public
	gzip -k public/FifthPawnChess.wasm

$(QT5_DIR)/5.15.2/gcc_64/bin/lrelease: | install-qt.sh $(QT5_DIR)
	./install-qt.sh --version 5.15.2 icu qttools qtbase --directory $(QT5_DIR)

$(QT5_DIR)/5.15.2/android/bin/qmake: | install-qt.sh $(QT5_DIR)
	./install-qt.sh --version 5.15.2 --toolchain any --target android tools qtbase qttranslations qtandroidextras qtsvg qtdeclarative qtquickcontrols2 --directory $(QT5_DIR)

$(NDK):
	curl -o android-ndk.zip https://dl.google.com/android/repository/android-ndk-r20-linux-x86_64.zip
	unzip  -q android-ndk.zip
	if  [[ ! -d "$(NDK)" ]]; then mv android-ndk "$(NDK)"; fi

$(SDK):
	curl -o android-sdk.zip https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip
	unzip -q android-sdk.zip -d $(SDK)

.ONESHELL:
$(SDK)/platforms/android-26: | $(SDK)
	$(SDK)/tools/bin/sdkmanager --update
	yes | $(SDK)/tools/bin/sdkmanager --licenses
	$(SDK)/tools/bin/sdkmanager "platforms;android-26"
	$(SDK)/tools/bin/sdkmanager "build-tools;28.0.2"

.ONESHELL:
FifthPawnChess.apk: | $(QT5_DIR)/5.15.2/android/bin/qmake $(QT5_DIR)/5.15.2/android/mkspecs/modules/qt_lib_networkauth.pri $(NDK) $(SDK)/platforms/android-26
	export ANDROID_SDK_ROOT=$(SDK) ANDROID_NDK_ROOT="$(NDK)"
	$(QT5_DIR)/5.15.2/android/bin/lrelease FifthPawnChess.pro
	$(QT5_DIR)/5.15.2/android/bin/qmake
	make
	make install INSTALL_ROOT=android-build && $(QT5_DIR)/5.15.2/android/bin/androiddeployqt --output android-build --input android-FifthPawnChess-deployment-settings.json --gradle $(ANDROIDDEPLOYQT_OPTS)
	(mv android-build/build/outputs/apk/release/android-build-release-signed.apk FifthPawnChess.apk || mv android-build/build/outputs/apk/release/android-build-release-unsigned.apk FifthPawnChess.apk)

pwd:=$(shell pwd)
wineenv=$(WINEPREFIX) $(WINEENV) WINEDEBUG=-all
winepath=$(wineenv) winepath
wine=$(WINEPATH) $(wineenv) wine
WINEPREFIX=WINEPREFIX="$(pwd)/wine"
WINEPATH=WINEPATH='C:\Program Files (x86)\CMake\bin;$(shell $(winepath) -w "$(QT5_DIR)/Tools/mingw810_64/bin");$(shell $(winepath) -w "$(QT5_DIR)/5.15.2/mingw81_64/bin")' QTDIR="Z:$(QT5_DIR)/5.15.2/mingw81_64/"
qmake=$(wine) $(QT5_DIR)/5.15.2/mingw81_64/bin/qmake.exe

.PHONY: build winbuild windeploy linuxdeploy

.ONESHELL:
build:
	lrelease-qt5 FifthPawnChess.pro
	mkdir -p build
	cd build
	qmake-qt5 .. -spec linux-g++
	make

linuxdeploy: build
	mkdir FifthPawnChess
	cp build/FifthPawnChess -f FifthPawnChess/FifthPawnChess

mingw.7z:
	wget -O mingw.7z http://download.qt.io/online/qtsdkrepository/windows_x86/desktop/tools_mingw/qt.tools.win64_mingw810/8.1.0-1-202004170606x86_64-8.1.0-release-posix-seh-rt_v6-rev0.7z

$(QT5_DIR)/5.15.2/mingw81_64/bin/qmake.exe: | install-qt.sh $(QT5_DIR)
	./install-qt.sh --host windows_x86 --version 5.15.2 --toolchain win64_mingw81 --target desktop tools qtbase qttranslations qtsvg qtdeclarative qtquickcontrols2 qtnetworkauth --directory $(QT5_DIR)

$(QT5_DIR)/5.15.2/mingw81_64/bin/qt.conf: $(QT5_DIR)/5.15.2/mingw81_64/bin/qmake.exe $(QT5_DIR)/Tools/mingw810_64
	echo "[paths]" > $(QT5_DIR)/5.15.2/mingw81_64/bin/qt.conf
	echo "Prefix=../" >> $(QT5_DIR)/5.15.2/mingw81_64/bin/qt.conf

$(QT5_DIR)/Tools/mingw810_64: mingw.7z
	7zr -o$(QT5_DIR)/ x mingw.7z
	touch -c $@

.ONESHELL:
build_win32/FifthPawnChess.exe: $(QT5_DIR)/5.15.2/mingw81_64/bin/qt.conf
	lrelease-qt5 FifthPawnChess.pro
	mkdir -p build_win32
	cd build_win32
	$(qmake) $(qmake_opts) ../FifthPawnChess.pro GIT_DESCRIBE="$(shell git describe --tags --long)"
	$(wine) mingw32-make

winbuild: build_win32/FifthPawnChess.exe

build_win32/release/FifthPawnChess.exe: winbuild

windeploy: build_win32/release/FifthPawnChess.exe
	mkdir -p FifthPawnChess/
	cp $(QT5_DIR)/Tools/mingw810_64/opt/bin/libeay32.dll -f FifthPawnChess/libeay32.dll
	cp $(QT5_DIR)/Tools/mingw810_64/opt/bin/ssleay32.dll -f FifthPawnChess/ssleay32.dll
	cp build_win32/release/FifthPawnChess.exe -f FifthPawnChess/
	$(wine) $(QT5_DIR)/5.15.2/mingw81_64/bin/windeployqt.exe --qmldir . FifthPawnChess/FifthPawnChess.exe
