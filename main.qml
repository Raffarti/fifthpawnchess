/***
 * Copyright 2021 Raffaele Pertile <raffarti@zoho.com>
 *
 * This file is part of raffarti.FifthPawnChess.
 *
 * raffarti.FifthPawnChess is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * raffarti.FifthPawnChess is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with raffarti.FifthPawnChess. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import FifthPawnChess 1

ApplicationWindow {
    width: 640
    height: 480
    visible: true
    title: qsTr("Emerald Chess")
    Board {
        anchors.fill: parent
    }
    Popup {
        anchors.centerIn: parent
        id: dialog
        modal: true
        focus: true
        visible: true
        closePolicy: Popup.NoAutoClose

        StackView {
            id: dialogStack
            clip: true
            anchors.fill: parent
            implicitHeight: Math.max(token.implicitHeight, goToken.implicitHeight, dialogGreet.implicitHeight)
            implicitWidth: Math.max(token.implicitWidth, goToken.implicitWidth, dialogGreet.implicitWidth)
            initialItem: ColumnLayout {
                id: dialogGreet
                Layout.fillWidth: true
                Layout.fillHeight: true
                Button {
                    text: qsTr("Play locally")
                    onClicked: dialog.close()
                }
                Button {
                    text: qsTr("Play on Lichess.org")
                    onClicked: {
                        if (!Engine.login())
                            dialogStack.push(goToken)
                    }
                    enabled: Engine.loginReady
                    BusyIndicator {
                        id: loginBusy
                        visible: !Engine.loginReady
                        anchors.fill: parent
                        Connections {
                            target: Engine
                            function onLoginResult(i) {
                                if (!i) {
                                    dialog.close()
                                } else {
                                    dialog.enabled = true
                                    loginBusy.visible = false
                                    if (i == 1)
                                        dialogStack.push(goToken)
                                    else
                                        dialogStack.pop(null)
                                }
                            }
                        }
                    }
                }
            }
            ColumnLayout {
                visible: false
                id: goToken
                Button {
                    text: qsTr("Get an access token from lichess.org")
                    onClicked: {
                        Engine.openUrl("https://lichess.org/account/oauth/token/create?scopes[]=board:play&description=Access+token+for+FifthPawnChess")
                        dialogStack.push(token)
                    }
                }
                Button {
                    text: qsTr("I already have a lichess.org access token")
                    onClicked: dialogStack.push(token)
                }
                Button {
                    text: qsTr("Back")
                    onClicked: dialogStack.pop()
                }
            }
            ColumnLayout {
                visible: false
                id: token
                TextField {
                    id: tokenField
                    onAccepted: access()
                    focus: true
                }
                Button {
                    text: qsTr("Access")
                    onClicked: token.access()
                }
                Button {
                    text: qsTr("Back")
                    onClicked: dialogStack.pop()
                }
                function access() {
                    Engine.login(tokenField.text)
                    dialogStack.pop(null)
                }
            }
        }
    }
}
