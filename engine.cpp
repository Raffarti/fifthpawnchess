/***
 * Copyright 2021 Raffaele Pertile <raffarti@zoho.com>
 *
 * This file is part of raffarti.FifthPawnChess.
 *
 * raffarti.FifthPawnChess is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * raffarti.FifthPawnChess is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with raffarti.FifthPawnChess. If not, see <http://www.gnu.org/licenses/>.
 */

#include "engine.h"
#include "game.h"
#include "pieces.h"
#include "state.h"

#include <QDesktopServices>
#include <QNetworkAccessManager>
#include <QOAuthHttpServerReplyHandler>

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QTimer>

#ifdef Q_OS_WASM
#include <emscripten.h>
#include <QFile>
#endif

#ifdef Q_OS_WASM
extern "C" {
    void EMSCRIPTEN_KEEPALIVE ready(Engine * engine) {
        engine->setLoginReady(true);
    }
    void EMSCRIPTEN_KEEPALIVE appendData(DoubleLineBuffer *r, char c) {
        r->append(c);
    }
    void EMSCRIPTEN_KEEPALIVE deleteLater(QObject *o) {
        o->deleteLater();
    }
    void EMSCRIPTEN_KEEPALIVE emitReadyRead(QIODevice *o) {
        emit o->readyRead();
    }
}
#endif

void Engine::setAccessToken(const QString &accessToken)
{
    m_accessToken = accessToken;
    saveToken(accessToken);
}

Engine::Engine(QObject *parent) : QObject(parent)
{
    qRegisterMetaType<ChessEnums::PieceId>("PieceId");
    qRegisterMetaType<ChessEnums::PieceType>("PieceType");
    qRegisterMetaType<Piece>("Piece");
    qRegisterMetaType<Pieces>("Pieces");
    qRegisterMetaType<Position>("Position");
    qRegisterMetaType<Positions>("Positions");
    m_game = new Game(this);
    m_nam = new NetworkAccessManager(this);
#ifdef Q_OS_WASM
    setLoginReady(false);
    EM_ASM(
        FS.mkdir('/data');
        FS.mount(IDBFS, {}, '/data');
        FS.syncfs(true, function (err) {
            _ready($0);
        });
    , this);
#else
    setLoginReady(true);
#endif
    connect(this, &Engine::loginResult, [this](int result){
        if (!result) {
            auto reply = m_auth->get({"https://lichess.org/api/account/playing"});
            connect(reply, &QNetworkReply::finished, [this, reply](){
                if (reply->error()) {
                    qDebug() << reply->error() << reply->readAll();
                    return;
                }
                auto asd = reply->readAll();
                QJsonDocument json = QJsonDocument::fromJson(asd);
                auto obj = json.object();
                auto games = obj["nowPlaying"].toArray();
                if (!games.isEmpty()) {
                    auto game = games[0].toObject();
                    if (game["color"].toString() == "white")
                        m_game->setActiveTeam(ChessEnums::Light);
                    else
                        m_game->setActiveTeam(ChessEnums::Dark);
                    auto gameId = game["gameId"].toString();
                    connect(m_game, &Game::playerMove, this, [this, gameId](ChessEnums::Team t, Position from, Position to, ChessEnums::PieceType extra){
                        assert(m_game->activeTeam() == ChessEnums::TeamBoth || t == m_game->activeTeam());
                        auto reply3 = m_auth->post({QString("https://lichess.org/api/board/game/%1/move/%2").arg(gameId).arg(encodeMove(from, to, extra))});
                        qDebug() << reply3->url();
                        connect(reply3, &QNetworkReply::finished, [reply3](){
                            qDebug() << reply3->url() << reply3->error() << reply3->readAll();
                            reply3->deleteLater();
                        });
                        connect(reply3, &QNetworkReply::errorOccurred, [reply3](){
                            qDebug() << reply3->url() << reply3->error() << reply3->readAll();
                            reply3->deleteLater();
                        });
                    });
                    auto jsonRead = [this](QByteArray text){
                        QJsonDocument json = QJsonDocument::fromJson(text);
                        if (json.isEmpty())
                            return;
                        auto obj = json.object();
                        if (obj["type"].toString() == "gameFull")
                            obj = obj["state"].toObject();
                        else if (obj["type"].toString() != "gameState")
                            return;
                        auto moves = obj["moves"].toString().split(' ', Qt::SkipEmptyParts);
                        for (int i = m_game->state()->turn; i < moves.size(); i++) {
                            auto [from, to, extra] = decodeMove(moves[i]);
                            auto piece = m_game->state()->cells[from.x()][from.y()];
                            m_game->tryMove(piece, to, extra);
                        }
                    };
#ifdef Q_OS_WASM
                    DoubleLineBuffer *buffer = new DoubleLineBuffer(this);
                    buffer->open(QIODevice::ReadOnly);
                    connect(buffer, &QIODevice::readyRead, [buffer, jsonRead](){
                        if (buffer->canReadLine())
                            jsonRead(buffer->readLine());
                    });
                    EM_ASM(
                                var headers = new Headers();
                                headers.append('Authorization', 'Bearer '+UTF8ToString($1));
                                fetch('https://lichess.org/api/board/game/stream/'+UTF8ToString($2), {method: 'GET', headers: headers})
                                .then(response => {
                                    const reader = response.body.getReader();
                                    function read() {
                                        reader.read()
                                        .then(({done, value}) => {if (done) _deleteLater($0); else { value.forEach(c => _appendData($0, c)); _emitReadyRead($0); return read(); }});
                                    }
                                    return read();
                                }).catch(error => console.debug(error));
                            , buffer, m_accessToken.toUtf8().data(), gameId.toUtf8().data());
#else
                    auto reply2 = m_auth->get({{QString("https://lichess.org/api/board/game/stream/%1").arg(gameId)}});
                    auto t = [this, reply2, jsonRead](){
                        if (!reply2->canReadLine())
                            return;
                        jsonRead(reply2->readLine());
                    };
                    connect(reply2, &QNetworkReply::readyRead, this, t);
                    connect(reply2, &QNetworkReply::finished, [reply2](){
                        qDebug() << reply2->url() << reply2->error() << reply2->readAll();
                        reply2->deleteLater();
                    });
                    connect(reply2, &QNetworkReply::errorOccurred, [reply2](){
                        qDebug() << reply2->url() << reply2->error() << reply2->readAll();
                        reply2->deleteLater();
                    });
#endif
                }

                reply->deleteLater();
            });
        }
    });
}

Game *Engine::game() const
{
    return m_game;
}

bool Engine::loginReady() const
{
    return m_loginReady;
}

std::tuple<Position, Position, ChessEnums::PieceType> Engine::decodeMove(QString move)
{
    if (move.length() == 4)
        return {Position{move[0].toLatin1() - 'a', move[1].digitValue() -1},
            Position{move[2].toLatin1() - 'a', move[3].digitValue() -1},
            ChessEnums::Dead
        };
    else
        return {Position{move[0].toLatin1() - 'a', move[1].digitValue() -1},
            Position{move[2].toLatin1() - 'a', move[3].digitValue() -1},
            ChessEnums::typeFromLetter(move[4])
        };
}

QString Engine::encodeMove(Position from, Position to, ChessEnums::PieceType extra)
{
    auto e = ChessEnums::letterForType(extra);
    if (e.isNull())
        return QString("%1%2%3%4").arg((char)('a' + from.x())).arg((char)('1' + from.y())).arg((char)('a' + to.x())).arg((char)('1' + to.y()));
    else
        return QString("%1%2%3%4%5").arg((char)('a' + from.x())).arg((char)('1' + from.y())).arg((char)('a' + to.x())).arg((char)('1' + to.y())).arg(e);
}

void Engine::setGame(Game *game)
{
    if (m_game == game)
        return;

    m_game = game;
    emit gameChanged(m_game);
}

PersistentOAuth2::PersistentOAuth2(QObject *parent) : QOAuth2AuthorizationCodeFlow(parent) {}

void PersistentOAuth2::restore(QString token)
{
    setToken(token);
    setStatus(Status::Granted);
}

bool Engine::login(QString token)
{
    if (!m_auth) {
        m_auth = new PersistentOAuth2(this);
        m_auth->setNetworkAccessManager(m_nam);
        m_auth->setClientIdentifier("uBJngi98gaq2k3e8");
        m_auth->setAccessTokenUrl({"https://oauth.lichess.org/oauth"});
        m_auth->setAuthorizationUrl({"https://oauth.lichess.org/oauth/authorize"});
        m_auth->setScope("board:play");

        m_auth->setModifyParametersFunction([](QAbstractOAuth::Stage stage, auto *parameters) {
            if (stage == QAbstractOAuth::Stage::RequestingAccessToken) {
                parameters->insert("client_secret", "KEOkD6PBhRn9cP1jgyeXXOqFZH252gIS");
            }
        });
    }
    auto replyHandler = new QOAuthHttpServerReplyHandler(('5'<<8)+'P', this);
    m_auth->setReplyHandler(replyHandler);
    if (token.isNull() && !replyHandler->isListening() && m_accessToken.isEmpty())
        return false;
    if (!token.isNull()) {
        setAccessToken(token);
    }
    connect(m_auth, &QOAuth2AuthorizationCodeFlow::authorizeWithBrowser, this, &Engine::openUrl);
    connect(m_auth, &QOAuth2AuthorizationCodeFlow::statusChanged, [this](){ m_authenticating = false; });
    connect(m_auth, &QOAuth2AuthorizationCodeFlow::granted, this, [this](){ setAccessToken(m_auth->token()); emit loginResult(0); });

    if (m_authenticating) return true;
    m_authenticating = true;
    if (m_auth->status() == QOAuth2AuthorizationCodeFlow::Status::NotAuthenticated){
        if (m_accessToken.isEmpty()) {
            m_auth->grant();
        }
        else {
            m_auth->restore(m_accessToken);
            emit loginResult(0);
        }
    }
    return true;
}

void Engine::openUrl(const QUrl &url)
{
#ifdef Q_OS_WASM
    EM_ASM({window.open(UTF8ToString($0), '_blank')}, url.toString().toUtf8().data());
#else
    QDesktopServices::openUrl(url);
#endif
}

void Engine::setLoginReady(bool loginReady)
{
    if (m_loginReady == loginReady)
        return;

    m_loginReady = loginReady;
    if (loginReady)
        m_accessToken = m_settings.value("accessToken").toString();

    emit loginReadyChanged(m_loginReady);
}

void Engine::importSettings(const QByteArray &data)
{
    QFile f(m_settings.fileName());
    f.open(QFile::WriteOnly);
    f.write(data);
    f.close();
}

void Engine::saveToken(QString token)
{
    m_settings.setValue("accessToken", token);
#ifdef Q_OS_WASM
    qDebug() << token;
    m_settings.sync();
    EM_ASM(
        FS.syncfs(function (err) {
        });
    );
#endif
}

#ifdef Q_OS_WASM
NetworkAccessManager::NetworkAccessManager(QObject *parent) : QNetworkAccessManager(parent)
{

}

QNetworkReply *NetworkAccessManager::createRequest(QNetworkAccessManager::Operation op, const QNetworkRequest &request, QIODevice *outgoingData)
{
    QNetworkRequest req(request);
    req.setHeader(QNetworkRequest::UserAgentHeader, {});
    return QNetworkAccessManager::createRequest(op, req, outgoingData);
}

qint64 DoubleLineBuffer::readData(char *data, qint64 maxlen)
{
    if (maxlen > c.size()) {
        memcpy(data, c.data(), c.size());
        maxlen = c.size();
        c.clear();
    } else {
        memcpy(data, c.data(), maxlen);
        c = c.right(maxlen);
    }
    return maxlen;
}

qint64 DoubleLineBuffer::writeData(const char *, qint64 )
{
    Q_UNREACHABLE();
}

DoubleLineBuffer::DoubleLineBuffer(QObject *parent) : QIODevice(parent)
{

}

void DoubleLineBuffer::append(char ch)
{
    c.append(ch);
}

qint64 DoubleLineBuffer::bytesAvailable() const
{
    return c.size();
}

bool DoubleLineBuffer::canReadLine() const
{
    return c.contains('\n');
}
#endif
