/***
 * Copyright 2021 Raffaele Pertile <raffarti@zoho.com>
 *
 * This file is part of raffarti.FifthPawnChess.
 *
 * raffarti.FifthPawnChess is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * raffarti.FifthPawnChess is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with raffarti.FifthPawnChess. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STATE_H
#define STATE_H

#include "pieces.h"
#include <QtQml>
#include <QHash>

using Positions = QHash<Position, ChessEnums::PieceId>;

class Move
{
    Q_GADGET
public:
    ChessEnums::PieceId piece;
    int x, y;
    int x0, y0;
};

class Promotion : Move
{
    Q_GADGET
public:
    ChessEnums::PieceType newType;
};

class Castling : Move
{
    Q_GADGET
public:
    ChessEnums::PieceType rook;
};

class State : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("")
    Q_PROPERTY(Pieces pieces READ pieces CONSTANT)
    Q_PROPERTY(int turn MEMBER turn CONSTANT)
    Pieces m_pieces;
public:
    explicit State(Pieces &&pieces, State *previous = nullptr, QObject *parent = nullptr) : QObject(parent), m_pieces(std::forward<Pieces>(pieces)), prev(previous), turn(prev?prev->turn +1:0)
    {
        std::fill(*cells, *cells + sizeof(cells)/sizeof(**cells), ChessEnums::None);
        for (int i = 0; i < 32; i++) {
            const auto &p = pieces[(ChessEnums::PieceId)i];
            if (!(p.type & ChessEnums::Dead))
                cells[p.pos.x()][p.pos.y()] = (ChessEnums::PieceId)i;
        }
    }
    Positions moves(ChessEnums::PieceId id) const;
    ChessEnums::PieceId cells[8][8];
    State * prev = nullptr;
    int turn = 0;
    Move move;
    Pieces pieces() const;
    bool tryPos(Positions& ret, const Position &pos, bool free, int eat) const;
    bool kCheck(int t) const;
    bool kCheck0(int t) const;
    bool kCheck1(int t) const;
    Q_INVOKABLE Piece piece(const Position &p);
    Q_INVOKABLE Piece piece(ChessEnums::PieceId id);
};

#endif // STATE_H
