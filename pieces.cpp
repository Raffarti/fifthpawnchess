/***
 * Copyright 2021 Raffaele Pertile <raffarti@zoho.com>
 *
 * This file is part of raffarti.FifthPawnChess.
 *
 * raffarti.FifthPawnChess is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * raffarti.FifthPawnChess is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with raffarti.FifthPawnChess. If not, see <http://www.gnu.org/licenses/>.
 */

#include "pieces.h"

Pieces Pieces::cleanSet()
{
    Pieces pieces;
    pieces.pieces[ChessEnums::LightLeftRook]    = { .type = ChessEnums::Rook,    .pos = {0, 0}};
    pieces.pieces[ChessEnums::LightLeftKnight]  = { .type = ChessEnums::Knight,  .pos = {1, 0}};
    pieces.pieces[ChessEnums::LightLeftBishop]  = { .type = ChessEnums::Bishop,  .pos = {2, 0}};
    pieces.pieces[ChessEnums::LightQueen]       = { .type = ChessEnums::Queen,   .pos = {3, 0}};
    pieces.pieces[ChessEnums::LightKing]        = { .type = ChessEnums::King,    .pos = {4, 0}};
    pieces.pieces[ChessEnums::LightRightBishop] = { .type = ChessEnums::Bishop,  .pos = {5, 0}};
    pieces.pieces[ChessEnums::LightRightKnight] = { .type = ChessEnums::Knight,  .pos = {6, 0}};
    pieces.pieces[ChessEnums::LightRightRook]   = { .type = ChessEnums::Rook,    .pos = {7, 0}};
    pieces.pieces[ChessEnums::LightPawn1]       = { .type = ChessEnums::Pawn,    .pos = {0, 1}};
    pieces.pieces[ChessEnums::LightPawn2]       = { .type = ChessEnums::Pawn,    .pos = {1, 1}};
    pieces.pieces[ChessEnums::LightPawn3]       = { .type = ChessEnums::Pawn,    .pos = {2, 1}};
    pieces.pieces[ChessEnums::LightPawn4]       = { .type = ChessEnums::Pawn,    .pos = {3, 1}};
    pieces.pieces[ChessEnums::LightPawn5]       = { .type = ChessEnums::Pawn,    .pos = {4, 1}};
    pieces.pieces[ChessEnums::LightPawn6]       = { .type = ChessEnums::Pawn,    .pos = {5, 1}};
    pieces.pieces[ChessEnums::LightPawn7]       = { .type = ChessEnums::Pawn,    .pos = {6, 1}};
    pieces.pieces[ChessEnums::LightPawn8]       = { .type = ChessEnums::Pawn,    .pos = {7, 1}};
    pieces.pieces[ChessEnums::DarkLeftRook]     = { .type = ChessEnums::Rook,    .pos = {0, 7}};
    pieces.pieces[ChessEnums::DarkLeftKnight]   = { .type = ChessEnums::Knight,  .pos = {1, 7}};
    pieces.pieces[ChessEnums::DarkLeftBishop]   = { .type = ChessEnums::Bishop,  .pos = {2, 7}};
    pieces.pieces[ChessEnums::DarkQueen]        = { .type = ChessEnums::Queen,   .pos = {3, 7}};
    pieces.pieces[ChessEnums::DarkKing]         = { .type = ChessEnums::King,    .pos = {4, 7}};
    pieces.pieces[ChessEnums::DarkRightBishop]  = { .type = ChessEnums::Bishop,  .pos = {5, 7}};
    pieces.pieces[ChessEnums::DarkRightKnight]  = { .type = ChessEnums::Knight,  .pos = {6, 7}};
    pieces.pieces[ChessEnums::DarkRightRook]    = { .type = ChessEnums::Rook,    .pos = {7, 7}};
    pieces.pieces[ChessEnums::DarkPawn1]        = { .type = ChessEnums::Pawn,    .pos = {0, 6}};
    pieces.pieces[ChessEnums::DarkPawn2]        = { .type = ChessEnums::Pawn,    .pos = {1, 6}};
    pieces.pieces[ChessEnums::DarkPawn3]        = { .type = ChessEnums::Pawn,    .pos = {2, 6}};
    pieces.pieces[ChessEnums::DarkPawn4]        = { .type = ChessEnums::Pawn,    .pos = {3, 6}};
    pieces.pieces[ChessEnums::DarkPawn5]        = { .type = ChessEnums::Pawn,    .pos = {4, 6}};
    pieces.pieces[ChessEnums::DarkPawn6]        = { .type = ChessEnums::Pawn,    .pos = {5, 6}};
    pieces.pieces[ChessEnums::DarkPawn7]        = { .type = ChessEnums::Pawn,    .pos = {6, 6}};
    pieces.pieces[ChessEnums::DarkPawn8]        = { .type = ChessEnums::Pawn,    .pos = {7, 6}};
    return pieces;
}

const Piece &Pieces::piece(int id)
{
    return pieces[id];
}

Piece &Pieces::operator[](ChessEnums::PieceId id)
{
    return pieces[id];
}
const Piece &Pieces::operator[](ChessEnums::PieceId id) const
{
    return pieces[id];
}

uint qHash(Position p)
{
    return p.x() + p.y() * 8;
}

bool Piece::neverMoved() const
{
    return moved == -1;
}

ChessEnums::PieceType ChessEnums::typeFromLetter(const QChar &c)
{
    if (c.isNull())
        return ChessEnums::Dead;
    switch (c.toLower().toLatin1()) {
    case 'r':
        return ChessEnums::Rook;
    case 'n':
        return ChessEnums::Knight;
    case 'b':
        return ChessEnums::Bishop;
    case 'q':
        return ChessEnums::Queen;
    case 'k':
        return ChessEnums::King;
    case 'p':
        return ChessEnums::Pawn;
    default:
        return ChessEnums::Dead;
    }
}

QChar ChessEnums::letterForType(ChessEnums::PieceType t)
{
    switch (t) {
    case ChessEnums::Rook:
        return 'r';
    case ChessEnums::Knight:
        return 'n';
    case ChessEnums::Bishop:
        return 'b';
    case ChessEnums::Queen:
        return 'q';
    case ChessEnums::King:
        return 'k';
    case ChessEnums::Pawn:
        return 'p';
    default:
        return {};
    }
}
