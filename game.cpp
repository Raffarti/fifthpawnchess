/***
 * Copyright 2021 Raffaele Pertile <raffarti@zoho.com>
 *
 * This file is part of raffarti.FifthPawnChess.
 *
 * raffarti.FifthPawnChess is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * raffarti.FifthPawnChess is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with raffarti.FifthPawnChess. If not, see <http://www.gnu.org/licenses/>.
 */

#include "game.h"
#include "state.h"
#include <QHash>
#include <QPair>

void Game::setWinner(int w)
{
    if (m_winner == w)
        return;

    m_winner = w;
    emit winnerChanged(w);
}

Game::Game(QObject *parent) : QObject(parent)
{
    m_state = new State(Pieces::cleanSet(), nullptr, this);
}

Player *Game::playerLight() const
{
    return m_playerLight;
}

Player *Game::playerDark() const
{
    return m_playerDark;
}

State *Game::state() const
{
    return m_state;
}

bool Game::tryMove(ChessEnums::PieceId piece, const Position &pos, ChessEnums::PieceType extra)
{
    auto state = moveState(piece, pos, extra);
    if (state) {
        state->setParent(this);
        state->prev = m_state;
        state->turn = m_state->turn +1;
        setState(state);
        return true;
    }
    return false;
}

void Game::playMove(ChessEnums::PieceId piece, const Position &pos, ChessEnums::PieceType extra)
{
    auto from = state()->piece(piece).pos;
    ChessEnums::Team t = (ChessEnums::Team)(state()->turn % 2);
    if (tryMove(piece, pos, extra))
        emit playerMove(t, from, pos, extra);
}

void Game::playMove(ChessEnums::PieceId piece, const Position &pos, int extra)
{
    playMove(piece, pos, (ChessEnums::PieceType)extra);
}

State * Game::moveState(ChessEnums::PieceId piece, const Position &pos, int extra) const
{
    auto t = piece >> 4;
    if (m_state->turn % 2 != (t))
        return nullptr;
    auto moves = m_state->moves(piece);
    if (!moves.contains(pos))
        return nullptr;
    Pieces ps = m_state->pieces();
    auto &p = ps[piece];
    p.pos = pos;
    auto opid = m_state->cells[pos.x()][pos.y()];
    if (opid != ChessEnums::None) {
        auto &op = ps[opid];
        op.type.setFlag(ChessEnums::Dead);
    }
    auto special = moves[pos];
    if ((special & ChessEnums::PieceMask) == ChessEnums::LightLeftRook) {
        auto s = moveState(piece, {pos.x() + 1, pos.y()});
        if (s)
            delete s;
        else
            return nullptr;
        auto & rook = ps[special];
        rook.moved = m_state->turn;
        rook.pos = {rook.pos.x() + 3, rook.pos.y()};
    }
    else if ((special & ChessEnums::PieceMask)  == ChessEnums::LightRightRook) {
        auto s = moveState(piece, {pos.x() - 1, pos.y()});
        if (s)
            delete s;
        else
            return nullptr;
        auto & rook = ps[special];
        rook.moved = m_state->turn;
        rook.pos = {rook.pos.x() - 2, rook.pos.y()};
    } else if (m_state->piece(special).type == ChessEnums::Pawn) {
        ps[special].type.setFlag(ChessEnums::Dead);
    }
    if (p.type == ChessEnums::Pawn && extra != ChessEnums::Dead &&
            ((t == ChessEnums::Light && p.pos.y() == 7) ||
             (t == ChessEnums::Dark && p.pos.y() == 0))) {
        p.type = (ChessEnums::PieceType)extra;
    }
    if (p.neverMoved())
        p.moved = m_state->turn;
    State *s = new State(std::move(ps));
    if ((p.type == ChessEnums::King && s->kCheck(t)) || (p.type != ChessEnums::King && s->kCheck0(t))) {
        delete s;
        return nullptr;
    }
    return s;
}

int Game::winner() const
{
    return m_winner;
}

int Game::status() const
{
    auto turn = m_state->turn % 2;
    bool stuck = true;
    for (int i = 0; i < 16; i++) {
        ChessEnums::PieceId id = (ChessEnums::PieceId)(i | (turn << 4));
        auto moves = m_state->moves(id);
        for (auto it = moves.begin(); it != moves.end(); it++) {
            auto s = moveState(id, it.key());
            if (s) {
                stuck = false;
                delete s;
                break;
            }
        }
        if (!stuck)
            break;
    }
    if (stuck) {
        if (m_state->kCheck(turn))
            return 1 - turn;
        else
            return 2;
    }
    stuck = true;
    for (int i = 0; i < 32; i++) {
        ChessEnums::PieceId id = (ChessEnums::PieceId)i;
        auto pstatus = m_state->piece(id).type;
        if ((uint)pstatus != ChessEnums::King && !(pstatus & ChessEnums::Dead))
            stuck = false;
    }
    if (stuck)
        return 2;
    return -1;
}

Positions Game::moves(ChessEnums::PieceId id)
{
    auto moves = m_state->moves(id);
    QMutableHashIterator<QPoint, ChessEnums::PieceId> it(moves);
    while (it.hasNext()){
        it.next();
        auto s = moveState(id, it.key());
        if (s)
            delete s;
        else
            it.remove();
    }
    return moves;
}

QVariantList Game::moves(int id)
{
    QVariantList ret;
    auto m = moves((ChessEnums::PieceId)id);
    for (auto it = m.begin(); it != m.end(); it++)
        ret << QVariant::fromValue(QVariantList{it.key(), it.value()});
    return ret;
}

ChessEnums::Team Game::activeTeam() const
{
    return m_activeTeam;
}

void Game::setPlayerLight(Player *playerLight)
{
    if (m_playerLight == playerLight)
        return;

    m_playerLight = playerLight;
    emit playerLightChanged(m_playerLight);
}

void Game::setPlayerDark(Player *playerDark)
{
    if (m_playerDark == playerDark)
        return;

    m_playerDark = playerDark;
    emit playerDarkChanged(m_playerDark);
}

void Game::setState(State *state)
{
    if (m_state == state)
        return;

    m_state = state;
    emit stateChanged(m_state);
    setWinner(status());
}

void Game::setActiveTeam(ChessEnums::Team activeTeam)
{
    if (m_activeTeam == activeTeam)
        return;

    m_activeTeam = activeTeam;
    emit activeTeamChanged(m_activeTeam);
}
