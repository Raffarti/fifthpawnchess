/***
 * Copyright 2021 Raffaele Pertile <raffarti@zoho.com>
 *
 * This file is part of raffarti.FifthPawnChess.
 *
 * raffarti.FifthPawnChess is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * raffarti.FifthPawnChess is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with raffarti.FifthPawnChess. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ENGINE_H
#define ENGINE_H

#include <QtQml>
#include "pieces.h"
#include <QSettings>
#include "game.h"

#ifdef Q_OS_WASM
#include <QNetworkAccessManager>

class NetworkAccessManager : public QNetworkAccessManager
{
    Q_OBJECT

public:
    NetworkAccessManager(QObject *parent = nullptr);
    // QNetworkAccessManager interface
protected:
    QNetworkReply *createRequest(Operation op, const QNetworkRequest &request, QIODevice *outgoingData) override;
};

class DoubleLineBuffer : public QIODevice {
    QByteArray c;

    // QIODevice interface
protected:
    qint64 readData(char *data, qint64 maxlen) override;
    qint64 writeData(const char *, qint64) override;
public:
    explicit DoubleLineBuffer(QObject *parent = nullptr);

    void append(char c);

    // QIODevice interface
public:
    qint64 bytesAvailable() const override;
    bool canReadLine() const override;
};

#else
class QNetworkAccessManager;
using NetworkAccessManager = QNetworkAccessManager;
#endif


#include <QOAuth2AuthorizationCodeFlow>

class PersistentOAuth2 : public QOAuth2AuthorizationCodeFlow {
    Q_OBJECT

public:
    explicit PersistentOAuth2(QObject *parent = nullptr);

public slots:

    void restore(QString token);
};

class Engine : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_SINGLETON
    Q_PROPERTY(Game * game READ game WRITE setGame NOTIFY gameChanged)
    Q_PROPERTY(bool loginReady READ loginReady WRITE setLoginReady NOTIFY loginReadyChanged)
    Game * m_game = nullptr;
    PersistentOAuth2 * m_auth = nullptr;
    NetworkAccessManager * m_nam = nullptr;
    bool m_authenticating = false;
    QSettings m_settings;

    bool m_loginReady;

    QString m_accessToken;

    void setAccessToken(const QString &accessToken);
public:
    explicit Engine(QObject *parent = nullptr);

    Game * game() const;

    bool loginReady() const;

    static std::tuple<Position, Position, ChessEnums::PieceType> decodeMove(QString move);
    static QString encodeMove(Position from, Position to, ChessEnums::PieceType extra);


public slots:
    void setGame(Game * game);

    bool login(QString token = {});

    void openUrl(const QUrl &url);

    void setLoginReady(bool loginReady);

    void importSettings(const QByteArray &data);

    void saveToken(QString token);

signals:

    void gameChanged(Game * game);

    void loginResult(int result);
    void loginReadyChanged(bool loginReady);
};
#endif // ENGINE_H
