/***
 * Copyright 2021 Raffaele Pertile <raffarti@zoho.com>
 *
 * This file is part of raffarti.FifthPawnChess.
 *
 * raffarti.FifthPawnChess is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * raffarti.FifthPawnChess is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with raffarti.FifthPawnChess. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PIECES_H
#define PIECES_H

#include <QObject>
#include <QtQml>

using Position = QPoint;

uint qHash(Position p);

class ChessEnums : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("")
public:
    enum PieceType {
        Rook,
        Knight,
        Bishop,
        Queen,
        King,
        Pawn,
        TypeMask = 0x0F,
        Dead = 0x10
    };
    Q_DECLARE_FLAGS(PieceStatus, PieceType)
    Q_FLAG(PieceStatus)
    enum Team {
        Light = 0x0,
        Dark = 0x1,
        TeamBoth,
        Spectator
    };
    Q_ENUM(Team);
    enum PieceId {
        None = -1,
        LightLeftRook,
        LightRightRook,
        LightLeftKnight,
        LightRightKnight,
        LightLeftBishop,
        LightRightBishop,
        LightQueen,
        LightKing,
        LightPawn1,
        LightPawn2,
        LightPawn3,
        LightPawn4,
        LightPawn5,
        LightPawn6,
        LightPawn7,
        LightPawn8,
        DarkLeftRook,
        DarkRightRook,
        DarkLeftKnight,
        DarkRightKnight,
        DarkLeftBishop,
        DarkRightBishop,
        DarkQueen,
        DarkKing,
        DarkPawn1,
        DarkPawn2,
        DarkPawn3,
        DarkPawn4,
        DarkPawn5,
        DarkPawn6,
        DarkPawn7,
        DarkPawn8,
        TeamMask = 0x10,
        PieceMask = 0x0F
    };
    Q_ENUM(PieceId)

    static ChessEnums::PieceType typeFromLetter(const QChar &c);
    static QChar letterForType(ChessEnums::PieceType t);
};

Q_DECLARE_OPERATORS_FOR_FLAGS(ChessEnums::PieceStatus)

class Piece
{
    Q_GADGET
    Q_PROPERTY(ChessEnums::PieceStatus type MEMBER type CONSTANT)
    Q_PROPERTY(Position pos MEMBER pos CONSTANT)
    Q_PROPERTY(bool moved MEMBER moved CONSTANT)
public:
    ChessEnums::PieceStatus type;
    Position pos;
    int moved = -1;
    bool neverMoved() const;
};

class Pieces
{
    Q_GADGET
    QML_ANONYMOUS
public:
    Piece pieces[32];

    static Pieces cleanSet();
    Q_INVOKABLE const Piece & piece(int id);

    Piece &operator[](ChessEnums::PieceId id);
    const Piece &operator[](ChessEnums::PieceId id) const;
};



#endif // PIECES_H
