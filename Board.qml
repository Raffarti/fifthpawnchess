/***
 * Copyright 2021 Raffaele Pertile <raffarti@zoho.com>
 *
 * This file is part of raffarti.FifthPawnChess.
 *
 * raffarti.FifthPawnChess is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * raffarti.FifthPawnChess is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with raffarti.FifthPawnChess. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import FifthPawnChess 1

Item {
    id: area
    property bool reversed: Engine.game.activeTeam == ChessEnums.Dark
    property var cells: ({})
    property real cellSize: Math.min(width, height) / 8
    property var highlights: []
    property var labels: ["a", "b", "c", "d", "e", "f", "g", "h"]
    property var activePiece
    property var targetCell
    function texture(type, team) {
        var teaml = (team == ChessEnums.Light) ? "l" : "d"
        var p
        switch(type & ChessEnums.TypeMask) {
        case ChessEnums.Rook:
            p = "r"
            break
        case ChessEnums.Knight:
            p = "n"
            break
        case ChessEnums.Bishop:
            p = "b"
            break
        case ChessEnums.Queen:
            p = "q"
            break
        case ChessEnums.King:
            p = "k"
            break
        case ChessEnums.Pawn:
            p = "p"
            break
        }
        return ("qrc:/Pieces/wiki/Chess_%1%2t45.svg").arg(p).arg(teaml)
    }

    Repeater {
        model: 64
        delegate: Rectangle {
            property int cellX: index % 8
            property int cellY: Math.floor(index / 8)
            property bool viable: area.highlights.filter(function(a) { return a[0].x == cellX && a[0].y == cellY}).length > 0
            color: (cellX + cellY) % 2  ? "lightgreen" : "#256025"
            width: parent.cellSize
            height: width
            x: (parent.reversed ? 7 - cellX : cellX) * width
            y: (parent.reversed ? cellY : 7 - cellY) * height
            Component.onCompleted: {parent.cells[index] = this; parent.cellsChanged()}
            Rectangle {
                radius: width
                width: parent.width * 0.5
                height: parent.width * 0.5
                anchors.centerIn: parent
                color: "black"
                opacity: 0.5
                visible: parent.viable
            }
        }
    }
    Rectangle {
        anchors.fill: area.targetCell
        color:   area.targetCell && area.targetCell.viable ? "yellow" : "red"
        anchors.margins: -2
        opacity: 0.25
        visible: area.targetCell ? area.activePiece && area.activePiece.cell != area.targetCell : false
    }
    Rectangle {
        color: "transparent"
        border.color: "yellow"
        border.width: 4
        anchors.margins: -2
        anchors.fill: area.activePiece && area.activePiece.cell
        visible: area.activePiece ? area.activePiece.cell : false
    }
    component CellLabel : Label {
        property var cell
        z: 1
        style: Text.Outline
        color: "Yellow"
        anchors.margins: 2
        anchors.leftMargin: 4
        anchors.rightMargin: 4
    }

    Repeater {
        model: 8
        CellLabel {
            cell: area.cells[area.reversed ? index : 63 - index]
            anchors.horizontalCenter: cell.horizontalCenter
            anchors.top: cell.top
            text: area.labels[!area.reversed ? 7 - index : index]
        }
    }
    Repeater {
        model: 8
        CellLabel {
            cell: area.cells[!area.reversed ? index : 63 - index]
            anchors.horizontalCenter: cell.horizontalCenter
            anchors.bottom: cell.bottom
            text: area.labels[area.reversed ? 7 - index : index]
        }
    }

    Repeater {
        model: 8
        CellLabel {
            cell: area.cells[area.reversed ? index * 8 : 63 - (index * 8)]
            anchors.verticalCenter: cell.verticalCenter
            anchors.right: cell.right
            text: (!area.reversed ? 7 - index : index) +1
        }
    }

    Repeater {
        model: 8
        CellLabel {
            cell: area.cells[!area.reversed ? index * 8 : 63 - (index * 8)]
            anchors.verticalCenter: cell.verticalCenter
            anchors.left: cell.left
            text: (area.reversed ? 7 - index : index) +1
        }
    }

    Repeater {
        model: 32
        Image {
            property var piece: Engine.game.state.piece(index)
            property int cellId: piece.pos.x + piece.pos.y * 8
            property var cell: parent.cells[cellId]
            property var cellTarget
            property int pieceId: index
            anchors.centerIn: cell
            width: cell ? cell.width * 0.8 : 10
            height: cell ? cell.height * 0.8 : 10
            source: parent.texture(piece.type, index >> 4)
            visible: !(Engine.game.state.piece(index).type & ChessEnums.Dead)
            mipmap: true
            z: area.activePiece == this ? 1 : 0
            MouseArea {
                enabled: area.activePiece == null || area.activePiece == parent
                anchors.fill: parent
                onPressed: function (mouse) {
                    mouse.accepted = true
                    parent.anchors.horizontalCenterOffset =  mouse.x - width/2
                    parent.anchors.verticalCenterOffset = mouse.y - height/2
                    area.highlights = Engine.game.moves(index)
                    area.activePiece = parent
                }
                onReleased: function (mouse) {
                    mouse.accepted = true
                    var mpos = mapToItem(area, mouse.x, mouse.y)
                    var ind = Math.floor(mpos.x / cell.width) + (7 - Math.floor(mpos.y / cell.height)) * 8
                    var cellTarget = parent.parent.cells[area.reversed ? 63 - ind : ind]
                    area.highlights = []
                    var myTurn = Engine.game.activeTeam == ChessEnums.TeamBoth || Engine.game.state.turn % 2 == Engine.game.activeTeam
                    if (myTurn && cellTarget && parent.piece.type == ChessEnums.Pawn && ((index >> 4) == ChessEnums.Light && cellTarget.cellY == 7 || (index >> 4) == ChessEnums.Dark && cellTarget.cellY == 0)) {
                        promotionMenu.caller = parent
                        parent.cellTarget = cellTarget
                        promotionMenu.x = Qt.binding(function() { return Math.min(mpos.x, area.width - promotionMenu.implicitWidth)})
                        promotionMenu.y = Qt.binding(function() { return Math.min(mpos.y, area.height - promotionMenu.implicitHeight)})
                        promotionMenu.open()
                    }
                    else {
                        if (cellTarget && myTurn)
                            Engine.game.playMove(index, Qt.point(cellTarget.cellX, cellTarget.cellY))
                        parent.anchors.horizontalCenterOffset = 0
                        parent.anchors.verticalCenterOffset = 0
                        area.activePiece = null
                        area.targetCell = null
                    }
                }
                onPositionChanged: function (mouse) {
                    parent.anchors.horizontalCenterOffset =  parent.anchors.horizontalCenterOffset + mouse.x - width/2
                    parent.anchors.verticalCenterOffset = parent.anchors.verticalCenterOffset + mouse.y - height/2
                    mouse.accepted = true
                    var mpos = mapToItem(area, mouse.x, mouse.y)
                    var cx = Math.floor(mpos.x / cell.width), cy = Math.floor(mpos.y / cell.height)
                    if (cx < 0 || cx > 7 || cy < 0 || cy > 7) {
                        area.targetCell = null
                        return
                    }
                    var ind = cx + (7 - cy) * 8
                    area.targetCell = parent.parent.cells[area.reversed ? 63 - ind : ind]
                }
            }
        }
    }
    Popup {
        id: promotionMenu
        property var caller
        padding: 0
        Column {
            spacing: 0
            Repeater {
                id: menuRep
                model: [ChessEnums.Queen, ChessEnums.Rook, ChessEnums.Bishop, ChessEnums.Knight]
                delegate: ToolButton {
                    width: area.cellSize * 0.8
                    height: area.cellSize * 0.8
                    display: AbstractButton.IconOnly
                    icon.source: area.texture(menuRep.model[index], promotionMenu.caller ? promotionMenu.caller.pieceId >> 4: 0)
                    onClicked: {
                        Engine.game.playMove(promotionMenu.caller.pieceId, Qt.point(promotionMenu.caller.cellTarget.cellX, promotionMenu.caller.cellTarget.cellY), menuRep.model[index])
                        promotionMenu.close()
                    }
                }
            }
        }
        onClosed: {
            promotionMenu.caller.anchors.horizontalCenterOffset = 0
            promotionMenu.caller.anchors.verticalCenterOffset = 0
            area.activePiece = null
            area.targetCell = null
        }
    }
    Popup {
        visible: Engine.game.winner != -1
        anchors.centerIn: area
        Label {
            text: {
                switch(Engine.game.winner) {
                case -1:
                    return ""
                case 0:
                    return qsTr("Light Won")
                case 1:
                    return qsTr("Dark Won")
                case 2:
                    return qsTr("Draw")
                }
            }
        }
    }
}
