QT += quick widgets network networkauth quickcontrols2

CONFIG += qmltypes
QML_IMPORT_NAME = FifthPawnChess
QML_IMPORT_MAJOR_VERSION = 1

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        engine.cpp \
        game.cpp \
        main.cpp \
        pieces.cpp \
        state.cpp

RESOURCES += qml.qrc

TRANSLATIONS += \
    FifthPawnChess_it_IT.ts

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    engine.h \
    game.h \
    pieces.h \
    state.h

android {
    QT += svg androidextras
}
wasm {
    QT += svg
    QMAKE_LFLAGS += -s FORCE_FILESYSTEM=1
    LIBS += -lidbfs.js
    QMAKE_RESOURCE_FLAGS += -no-compress
}

DISTFILES += \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/gradle.properties \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/res/values/libs.xml

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
android: include(/home/raffarti/Qt/openssl-OpenSSL_1_1_1g/openssl.pri)
