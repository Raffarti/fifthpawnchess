/***
 * Copyright 2021 Raffaele Pertile <raffarti@zoho.com>
 *
 * This file is part of raffarti.FifthPawnChess.
 *
 * raffarti.FifthPawnChess is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * raffarti.FifthPawnChess is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with raffarti.FifthPawnChess. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GAME_H
#define GAME_H

#include <QObject>
#include <QtQml>
#include "pieces.h"
#include "state.h"

class State;

class Player : public QObject
{
    Q_OBJECT
public:
    QString name;
};

class Game : public QObject
{
    Q_OBJECT
    QML_ANONYMOUS
    Q_PROPERTY(State * state READ state WRITE setState NOTIFY stateChanged)
    Q_PROPERTY(Player * playerLight READ playerLight WRITE setPlayerLight NOTIFY playerLightChanged)
    Q_PROPERTY(Player * playerDark READ playerDark WRITE setPlayerDark NOTIFY playerDarkChanged)
    Q_PROPERTY(ChessEnums::Team activeTeam READ activeTeam WRITE setActiveTeam NOTIFY activeTeamChanged)
    Q_PROPERTY(int winner READ winner NOTIFY winnerChanged)

    Player * m_playerLight = nullptr;
    Player * m_playerDark = nullptr;
    State * m_state = nullptr;

    int m_winner = -1;

    void setWinner(int w);
    ChessEnums::Team m_activeTeam = ChessEnums::TeamBoth;

public:
    explicit Game(QObject *parent = nullptr);

    Player * playerLight() const;

    Player * playerDark() const;

    State * state() const;

    bool tryMove(ChessEnums::PieceId piece, const Position & pos, ChessEnums::PieceType extra = ChessEnums::Dead);
    void playMove(ChessEnums::PieceId piece, const Position & pos, ChessEnums::PieceType extra);
    Q_INVOKABLE void playMove(ChessEnums::PieceId piece, const Position & pos, int extra = ChessEnums::Dead);
    State * moveState(ChessEnums::PieceId piece, const Position & pos, int extra = ChessEnums::Dead) const;

    int winner() const;

    int status() const;

    Positions moves(ChessEnums::PieceId id);
    Q_INVOKABLE QVariantList moves(int id);

    ChessEnums::Team activeTeam() const;

public slots:
    void setPlayerLight(Player * playerLight);

    void setPlayerDark(Player * playerDark);

    void setState(State * state);

    void setActiveTeam(ChessEnums::Team activeTeam);

signals:

    void playerLightChanged(Player * playerLight);
    void playerDarkChanged(Player * playerDark);
    void stateChanged(State * state);
    void winnerChanged(int winner);
    void playerMove(ChessEnums::Team, Position from, Position pos, ChessEnums::PieceType extra = ChessEnums::Dead);
    void activeTeamChanged(ChessEnums::Team activeTeam);
};

#endif // GAME_H
