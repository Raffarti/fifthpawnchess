/***
 * Copyright 2021 Raffaele Pertile <raffarti@zoho.com>
 *
 * This file is part of raffarti.FifthPawnChess.
 *
 * raffarti.FifthPawnChess is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * raffarti.FifthPawnChess is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with raffarti.FifthPawnChess. If not, see <http://www.gnu.org/licenses/>.
 */

#include "state.h"
#include <QDebug>

Pieces State::pieces() const
{
    return m_pieces;
}

Positions State::moves(ChessEnums::PieceId piece) const
{
    Positions ret;
    auto pos = m_pieces[piece].pos;
    switch(m_pieces[piece].type) {
    case ChessEnums::Rook: {
        static const Position rookMoves[] = {{-1, 0}, {1, 0}, {0, 1}, {0, -1}};
        for (auto p: rookMoves)
            for (int i = 1; i < 8; i++) {
                Position p1 {pos.x() + p.x() * i, pos.y() + p.y() * i};
                if (tryPos(ret, p1, true, piece >> 4))
                    break;
            }
        break;
    }
    case ChessEnums::Knight: {
        static const Position knightMoves[] = {{1, 2}, {2, 1},
                                       {-1, 2}, {2, -1},
                                       {-1, -2}, {-2, -1},
                                       {1, -2}, {-2, 1},
                                      };
        for (auto p: knightMoves)
            tryPos(ret, {pos.x() + p.x(), pos.y() + p.y()}, true, piece >> 4);
        break;
    }
    case ChessEnums::Bishop: {
        static const Position bishopMoves[] = {{-1, -1}, {1, 1}, {-1, 1}, {1, -1}};
        for (auto p: bishopMoves)
            for (int i = 1; i < 8; i++) {
                Position p1 {pos.x() + p.x() * i, pos.y() + p.y() * i};
                if (tryPos(ret, p1, true, piece >> 4))
                    break;
            }
        break;
    }
    case ChessEnums::Queen: {
        static const Position queenMoves[] = {{-1, 0}, {1, 0}, {0, 1}, {0, -1}, {-1, -1}, {1, 1}, {-1, 1}, {1, -1}};
        for (const Position &p: queenMoves)
            for (int i = 1; i < 8; i++) {
                Position p1 {pos.x() + p.x() * i, pos.y() + p.y() * i};
                if (tryPos(ret, p1, true, piece >> 4))
                    break;
            }
        break;
    }
    case ChessEnums::King: {
        static const Position kingMoves[] = {{1, 1}, {0, 1},
                                       {-1, 1}, {1, 0},
                                       {-1, -1}, {0, -1},
                                       {1, -1}, {-1, 0},
                                      };
        auto t = piece >> 4;
        for (auto p: kingMoves)
            tryPos(ret, {pos.x() + p.x(), pos.y() + p.y()}, true, t);
        auto lr = (ChessEnums::PieceId) (ChessEnums::LightLeftRook | (t<<4));
        auto rr = (ChessEnums::PieceId) (ChessEnums::LightRightRook | (t<<4));
        if (m_pieces[piece].neverMoved() && m_pieces[lr].neverMoved() &&
                cells[pos.x() -1][pos.y()] == ChessEnums::None && cells[pos.x() -2][pos.y()] == ChessEnums::None && cells[pos.x() -3][pos.y()] == ChessEnums::None && !kCheck(piece >> 4))
            ret[{pos.x()-2, pos.y()}] = lr;
        if (m_pieces[piece].neverMoved() && m_pieces[rr].neverMoved() &&
                cells[pos.x() +1][pos.y()] == ChessEnums::None && cells[pos.x() +2][pos.y()] == ChessEnums::None && !kCheck(piece >> 4))
            ret[{pos.x()+2, pos.y()}] = rr;
        break;
    }
    case ChessEnums::Pawn: {
        static const Position pawnAttacks[] = {{1,1}, {-1,1}};
        auto c = piece >> 4 ? -1 : 1;
        for (auto p: pawnAttacks)
            tryPos(ret, {pos.x() + p.x(), pos.y() + p.y() * c}, false, piece >> 4);

        if (!tryPos(ret, {pos.x(), pos.y() + c}, true, -1) && m_pieces[piece].neverMoved())
            tryPos(ret, {pos.x(), pos.y() + 2 * c}, true, -1);

        for (auto p: pawnAttacks) {
            auto x = pos.x() + p.x(), y = pos.y();
            if (x < 0 || x > 7) continue;
            auto other = cells[x][y];
                if (other != ChessEnums::None) {
                    auto rp = m_pieces[other];
                    if (rp.type == ChessEnums::Pawn && rp.moved >= 0 && rp.moved == turn -1)
                        ret[{x, y + c}] = other;
                }
            }
    }
    default:;
    }
    return ret;
}

bool State::tryPos(Positions &ret, const Position &pos, bool free, int eat) const
{
    if (pos.x() < 0 || pos.x() > 7 || pos.y() < 0 || pos.y() > 7)
        return true;
    auto p = cells[pos.x()][pos.y()];
    if (free && p == ChessEnums::None) {
        ret[pos] = ChessEnums::None;
        return false;
    } else if (eat >= 0 && p != ChessEnums::None && p >> 4 != eat) {
        ret[pos] = ChessEnums::None;
        return true;
    }
    return true;
}

bool State::kCheck(int t) const
{
    return kCheck0(t) || kCheck1(t);
}

bool State::kCheck0(int t) const
{
    static const struct {Position pos[4]; std::function<bool(int)> check;} check0[] = {
        {
            {{-1, 0}, {1, 0}, {0, 1}, {0, -1}},
            [](int t){return t == ChessEnums::Rook || t ==  ChessEnums::Queen;}
        }, {
            {{-1, -1}, {1, 1}, {-1, 1}, {1, -1}},
            [](int t){return t == ChessEnums::Bishop || t == ChessEnums::Queen;}
        }
    };
    const Position &pos = m_pieces[(ChessEnums::PieceId) (ChessEnums::LightKing | (t<<4))].pos;
    for (const auto &check0p : check0) {
        for (const Position &p: check0p.pos) {
            for (int i = 1; i < 8; i++) {
                Position npos = {pos.x()+p.x()*i, pos.y()+p.y()*i};
                if (npos.x() < 0 || npos.x() > 7 || npos.y() < 0 || npos.y() > 7)
                    break;
                auto id = cells[npos.x()][npos.y()];
                if (id == ChessEnums::None) continue;
                if (id >> 4 == t) break;
                if (check0p.check(m_pieces[id].type))
                    return true;
                else
                    break;
            }
        }
    }
    return false;
}

bool State::kCheck1(int t) const
{
    static const struct {QVector<Position> pos; std::function<bool(int)> check;} check0[] = {
        {
            {{1, 1}, {-1, 1}},
            [](int t){return ChessEnums::Pawn == t;}
        }, {
            {{1, 2}, {2, 1}, {-1, 2}, {2, -1}, {-1, -2}, {-2, -1}, {1, -2}, {-2, 1}},
            [](int t){return t == ChessEnums::Knight;}
        }, {
            {{1, 1}, {0, 1}, {-1, 1}, {1, 0}, {-1, -1}, {0, -1}, {1, -1}, {-1, 0}},
            [](int t){return t == ChessEnums::King;}
        }
    };
    const Position &pos = m_pieces[(ChessEnums::PieceId) (ChessEnums::LightKing | (t<<4))].pos;
    for (const auto &check0p : check0) {
        for (const Position &p: check0p.pos) {
            auto c = t ? -1 : 1;
            Position npos = {pos.x()+p.x(), pos.y() + p.y() * c};
            if (npos.x() < 0 || npos.x() > 7 || npos.y() < 0 || npos.y() > 7)
                continue;
            auto id = cells[npos.x()][npos.y()];
            if (id == ChessEnums::None || id >> 4 == t) continue;
            if (check0p.check(m_pieces[id].type)) {
                return true;
            }
        }
    }
    return false;
}

Piece State::piece(const Position &p)
{
    return m_pieces.pieces[cells[p.x()][p.y()]];
}

Piece State::piece(ChessEnums::PieceId id)
{
    return m_pieces[id];
}
